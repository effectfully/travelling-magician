module Main where

import           TravellingMagician.Data
import qualified TravellingMagician.Search.Naive as Naive
import qualified TravellingMagician.Search.Smart as Smart
import           Data.List
import           Data.Fixed
import           Control.Monad
import           Test.QuickCheck

sortNub :: Ord a => [a] -> [a]
sortNub = map head . group . sort

newtype ShortRewardPointList = ShortRewardPointList
  { getShortRewardPointList :: [RewardPoint]
  } deriving (Show)

instance Arbitrary RewardPoint where
  arbitrary
     =  RewardPoint
    <$> ((\x -> x `mod'` 40) <$> arbitrary)
    <*> ((\x -> x `mod'` 40) <$> arbitrary)
    <*> ((\x -> abs x `rem` 10 + 1) <$> arbitrary)

instance Arbitrary ShortRewardPointList where
  arbitrary = ShortRewardPointList <$> do
    len <- choose (0, 6)
    replicateM len arbitrary

prop_NaiveSmartSameResults :: ShortRewardPointList -> Bool
prop_NaiveSmartSameResults (ShortRewardPointList xs) =
  sortNub (Naive.magicianSearchResults xs) == sortNub (Smart.magicianSearchResults xs)

main :: IO ()
main = quickCheck prop_NaiveSmartSameResults
