module Main where

import           TravellingMagician.Data
import           TravellingMagician.Search.Smart
import           Text.Read (readMaybe)
import           Control.Monad (guard)
import           System.Environment
import           System.Exit

parseRewardPoint :: String -> Maybe RewardPoint
parseRewardPoint str = do
  [x, y, r] <- Just $ words str
  RewardPoint <$> readMaybe x <*> readMaybe y <*> readMaybe r

parseRewardPoints :: String -> Maybe [RewardPoint]
parseRewardPoints contents = do
  lenStr : rewardPointsStr <- Just $ lines contents
  len <- readMaybe lenStr
  guard $ length rewardPointsStr == len
  traverse parseRewardPoint rewardPointsStr

main :: IO ()
main = do
  args <- getArgs
  case args of
    [inputFile] -> do
      mayRewardPoints <- parseRewardPoints <$> readFile inputFile
      case mayRewardPoints of
        Nothing           -> exitWith $ ExitFailure 1
        Just rewardPoints -> print $ magicianMaxSearchResult rewardPoints
    _           -> putStrLn "please provide a filename as an argument"
