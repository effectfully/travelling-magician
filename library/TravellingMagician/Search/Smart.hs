module TravellingMagician.Search.Smart
  ( magicianSearchResults
  , magicianMaxSearchResult
  ) where

import           TravellingMagician.Prelude
import           TravellingMagician.Data
import           TravellingMagician.Search.Smart.Data
import qualified Data.Vector as Vector
import qualified Data.IntSet as IntSet
import qualified Data.HashMap.Strict as HashMap

isPruneableUnorderedPath :: Double -> UnorderedPathValue -> Bool
isPruneableUnorderedPath dist path = _unorderedPathCost path > dist

appendVertex :: MagicianEnvironment -> (UnorderedPath, UnorderedPathValue) -> Int -> (UnorderedPath, UnorderedPathValue)
appendVertex (MagicianEnvironment _ rewards distanceMatrix) (path, pathValue) recent = (path', pathValue') where
  UnorderedPath passed current            = path
  UnorderedPathValue cost earned unpassed = pathValue
  cost'     = cost + distanceMatrix Vector.! current Vector.! recent
  earned'   = earned + rewards Vector.! _unorderedPathCurrent path
  unpassed' = IntSet.delete recent unpassed
  path'      = UnorderedPath (IntSet.insert current passed) recent
  pathValue' = UnorderedPathValue cost' earned' unpassed'

expandValuedUnorderedPath :: MagicianEnvironment -> (UnorderedPath, UnorderedPathValue) -> [(UnorderedPath, UnorderedPathValue)]
expandValuedUnorderedPath magEnv pathPair =
  map (appendVertex magEnv pathPair) . IntSet.toList . _unorderedPathUnpassed $ snd pathPair

checkValuedUnorderedPath :: (UnorderedPath, UnorderedPathValue) -> Maybe Earned
checkValuedUnorderedPath (path, pathValue) = do
  guard $ _unorderedPathCurrent path == 0
  Just $ _unorderedPathEarned pathValue

partitionByFinished :: [(UnorderedPath, UnorderedPathValue)] -> ([Earned], [(UnorderedPath, UnorderedPathValue)])
partitionByFinished
  = partitionEithers
  . map (\valuedPath -> maybe (Right valuedPath) Left $ checkValuedUnorderedPath valuedPath)

pruneValuedUnorderedPaths :: Double -> [(UnorderedPath, UnorderedPathValue)] -> [(UnorderedPath, UnorderedPathValue)]
pruneValuedUnorderedPaths dist = filter $ not . isPruneableUnorderedPath dist . snd

expandSearchSpace :: MagicianEnvironment -> UnorderedSearchState -> Maybe [(UnorderedPath, UnorderedPathValue)]
expandSearchSpace magEnv =
  returnNonEmpty . concatMap (expandValuedUnorderedPath magEnv) . HashMap.toList

reduceSearchSpace :: [(UnorderedPath, UnorderedPathValue)] -> UnorderedSearchState
reduceSearchSpace = HashMap.fromListWith min

magicianSearchStep :: MagicianEnvironment -> UnorderedSearchState -> Maybe ([Earned], UnorderedSearchState)
magicianSearchStep magEnv unorderedSearchState
   = (second reduceSearchSpace . partitionByFinished . pruneValuedUnorderedPaths (_magicianMovingDistance magEnv))
  <$> expandSearchSpace magEnv unorderedSearchState

magicianSearchResults :: [RewardPoint] -> [Earned]
magicianSearchResults points =
  let clique = toEuclideanRewardClique points
  in concat $ unfoldr (magicianSearchStep $ makeMagicianEnvironment clique) (initialUnorderedSearchState clique)

magicianMaxSearchResult :: [RewardPoint] -> Earned
magicianMaxSearchResult = maximum . magicianSearchResults
