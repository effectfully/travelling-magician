module TravellingMagician.Search.Smart.Data where

import           TravellingMagician.Prelude
import           TravellingMagician.Data
import qualified Data.Vector as Vector
import qualified Data.IntSet as IntSet
import qualified Data.HashMap.Strict as HashMap

type EuclideanRewardClique = Vector RewardPoint

type VertexRewards  = Vector Int
type DistanceMatrix = Vector (Vector Double)

data MagicianEnvironment = MagicianEnvironment
  { _magicianMovingDistance :: Double
  , _magicianVertexRewards  :: VertexRewards
  , _magicianDistanceMatrix :: DistanceMatrix
  }

data UnorderedPath = UnorderedPath
  { _unorderedPathPassed  :: IntSet
  , _unorderedPathCurrent :: Int
  } deriving (Eq, Generic)

type Earned           = Int
type UnpassedVertices = IntSet

-- Add `[Int]` in order to save paths.
data UnorderedPathValue = UnorderedPathValue
  { _unorderedPathCost     :: Double
  , _unorderedPathEarned   :: Earned
    -- This eats a lot of memory. Does it worth it? We can compute unpassed vertices on the fly.
  , _unorderedPathUnpassed :: UnpassedVertices
  } deriving (Eq, Ord)

type UnorderedSearchState = HashMap UnorderedPath UnorderedPathValue

instance Hashable UnorderedPath

toEuclideanRewardClique :: [RewardPoint] -> EuclideanRewardClique
toEuclideanRewardClique points = Vector.fromList $ RewardPoint 0 0 0 : points

computeDistanceMatrix :: EuclideanRewardClique -> DistanceMatrix
computeDistanceMatrix clique = withPoint $ \p1 -> withPoint $ \p2 -> distance p1 p2 where
  withPoint k = Vector.map (k . fromRewardPoint) clique

makeMagicianEnvironment :: EuclideanRewardClique -> MagicianEnvironment
makeMagicianEnvironment clique
  = MagicianEnvironment constantMagicianDistance (Vector.map _rewardPointWeight clique)
  $ computeDistanceMatrix clique

initialUnorderedSearchState :: EuclideanRewardClique -> UnorderedSearchState
initialUnorderedSearchState clique
  = HashMap.singleton (UnorderedPath mempty 0)
  $ UnorderedPathValue 0 0 (IntSet.fromList [0 .. Vector.length clique - 1])
