module TravellingMagician.Search.Naive
  ( magicianSearchResults
  , magicianMaxSearchResult
  ) where

import           TravellingMagician.Prelude
import           TravellingMagician.Data

allPossiblePaths :: [RewardPoint] -> [[RewardPoint]]
allPossiblePaths
  = map (\path -> [initial] ++ path ++ [initial])
  . concatMap subsequences
  . permutations
  where initial = RewardPoint 0 0 0

pathCost :: [RewardPoint] -> Double
pathCost path = sum $ zipWith (distance `on` fromRewardPoint) path (tail path)

pathEarned :: [RewardPoint] -> Maybe Int
pathEarned path = do
  guard . not $ pathCost path > constantMagicianDistance
  Just . sum $ map _rewardPointWeight path

magicianSearchResults :: [RewardPoint] -> [Int]
magicianSearchResults = mapMaybe pathEarned . allPossiblePaths

magicianMaxSearchResult :: [RewardPoint] -> Int
magicianMaxSearchResult = maximum . magicianSearchResults
