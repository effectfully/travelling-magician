module TravellingMagician.Prelude
  ( module Export
  , module TravellingMagician.Prelude
  ) where

import           Data.Function       as Export
import           Data.Maybe          as Export
import           Data.Either         as Export
import           Data.List           as Export
import           Data.Traversable    as Export
import           Data.Vector         as Export (Vector)
import           Data.IntSet         as Export (IntSet)
import           Data.Hashable       as Export
import           Data.HashMap.Strict as Export (HashMap)
import           Control.Monad       as Export
import           Control.Arrow       as Export
import           GHC.Generics        as Export

import qualified Data.IntSet as IntSet

instance Hashable IntSet where
  hashWithSalt salt = hashWithSalt salt . IntSet.toList

returnNonEmpty :: [a] -> Maybe [a]
returnNonEmpty [] = Nothing
returnNonEmpty xs = Just xs

distance :: (Double, Double) -> (Double, Double) -> Double
distance (x1, y1) (x2, y2) = sqrt $ dist x1 x2 + dist y1 y2 where
  dist z1 z2 = (z2 - z1) ^ (2 :: Int)

picksList :: [a] -> [(a, [a])]
picksList  []      = []
picksList (x : xs) = (x, xs) : map (second (x :)) (picksList xs)
