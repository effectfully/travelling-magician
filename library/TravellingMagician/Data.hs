module TravellingMagician.Data where

constantMagicianDistance :: Double
constantMagicianDistance = 15 * 8

data RewardPoint = RewardPoint
  { _rewardPointX      :: Double
  , _rewardPointY      :: Double
  , _rewardPointWeight :: Int
  } deriving (Show)

fromRewardPoint :: RewardPoint -> (Double, Double)
fromRewardPoint (RewardPoint x y _) = (x, y)
