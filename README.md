# travelling-magician

In order to build, run `stack build`.

In order to execute, run `stack exec -- travelling-magician <filename>`.

## The problem

You are a magician, master of a very useful spell: you can
instantaneously cure any illness of anybody you touch. Each morning
you look into your scrying pool to see who is sick today, and how
serious. Those who you will not have time to cure will have to visit a
regular doctor.

You would listen to your heart and rush to the most seriously ill
first: physicians around here are… well, you wouldn't trust them with
anything but the most benign cases. But the truth is that the Count
has offered to pay you a fee for each person you cure, and you need
the money: your tower _really_ needs refurbishing.

Sure, he will give you more for curing serious illness, but the county
is large, even for you who can fly, and it may be worth favouring
quantity over quality.

Your goal is to write a program that will maximise the amount of money
you can collect in a single day by optimising the patient you will
choose to cure.

Patients are located all over the county, fortunately you can fly over
the county in straight lines at a speed of 15km/h.

Patients are assigned a seriousness from 1 to 10. The count will pay
you _x_ florins for curing a patient with seriousness _x_.

Every morning you leave from your tower, and you must be back to your
tower at the end of your 8h work day.

### Input

The main input to the program is a `*.map` file, representing today's
patient. It has the following format:

* The first line is a single integer _N_, giving the number of patients
  available today
* all following lines in the file are triples consisting in a pair of coordinates, _i.e._
  a pair of floating point numbers in kilometers, and an integer representing the
  seriousness. The triple is separated by space characters. The
  number of such triples in the file is given by _N_.

The coordinate system is x,y based, with the origin located at your
tower (_i.e._ your tower is always at coordinate _(0,0)_). Coordinates
may include negative numbers.

## Analysis

The problem is clearly NP-complete. Since it is not required to visit all vertices of a graph, we can't use some branch and bound strategy. And the fact that the space is Euclidean doesn't help us much. So it seems we can only write some breadth-first search with pruning.

We have two search strategies.

A naive strategy uses brute-force in order to generate all possible paths and then extract all legal ones (i.e. shorter than `15 * 8` km), count how much they earn you and put those values into a list.

A smart strategy is a bit more interesting. As simple optimizations we do two things: precompute distances between all pairs of points (as `sqrt` is a rather expensive operation) and prune paths that are longer than the maximal distance magician is able to pass. To understand a more involved optimization we perform, consider these two diagrams:

```
x1----------x2        x1          x2
           /          |          /|
          /           |         / |
         /            |        /  |
        /             |       /   |
       /              |      /    |
      /               |     /     |
     /                |    /      |
    /                 |   /       |
   /                  |  /        |
  /                   | /         |
 /                    |/          |
x3----------x4        x3          x4
```

So the magician starts from `x1` and ends at `x4` in both the cases. But clearly in the latter case the magician passes more distance as the path from `x1` to `x3` is longer than the path from `x1` to `x2` and the same holds for the `x3`-to-`x4` and `x2`-to-`x4` pairs. So the optimization is to prune pathes when there are more optimal ones which consist of the same points and end in the same destination. In code this is expressed as follows:

```haskell
data UnorderedPath = UnorderedPath
  { _unorderedPathPassed  :: IntSet
  , _unorderedPathCurrent :: Int
  }

data UnorderedPathValue = UnorderedPathValue
  { _unorderedPathCost     :: Double
  , _unorderedPathEarned   :: Int
  , _unorderedPathUnpassed :: IntSet
  } deriving (Eq, Ord)

type UnorderedSearchState = HashMap UnorderedPath UnorderedPathValue
```

So we have a `HashMap` from unordered paths to path values each of which consists of the distance passed so far, total gain and an unpassed part of the graph. At each step we expand every path in this `HashMap` with a new point on the graph and then reduce by choosing only optimal paths collected so far. And we also prune too expensive paths in the middle. In the language of type signatures this looks as


```haskell
pruneValuedUnorderedPaths :: Double -> [(UnorderedPath, UnorderedPathValue)] -> [(UnorderedPath, UnorderedPathValue)]

expandSearchSpace :: MagicianEnvironment -> UnorderedSearchState -> Maybe [(UnorderedPath, UnorderedPathValue)]

reduceSearchSpace :: [(UnorderedPath, UnorderedPathValue)] -> UnorderedSearchState

magicianSearchStep :: MagicianEnvironment -> UnorderedSearchState -> Maybe ([Earned], UnorderedSearchState)
```

`magicianSearchStep` also outputs a list of costs of successful solutions which have been found at a current step. It returns `Nothing` when `UnorderedSearchState` becomes empty (i.e. on exhausted search space).

And then we apply `unfoldr` to `magicianSearchStep` thus achieving lazy streaming of intermediate solutions. The best solution is computed as the maximum of said stream.

A possible optimization would be to make the algorithm bidirectional like they do in bidirectional Dijkstra, but it's likely that the current implementation can be parallelized just as well, since a lot of operations are run over elements of lists and are independent of each other.

For tests we simply generate random small graphs and check whether the naive and the smart algorithms agree on the results. It's hard to write verifying unit tests here, because it's hard to look at a graph and say what solution is optimal.